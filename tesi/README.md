# Live elettronics atteggiamento ecosistemico

## Descrittori

- spread spettrale
- inviluppo spettrale
- envelope following

### quelli scelti ad ora e perché

- envelope following

## Algoritmo adattivo

- Utilizzo midside per trattamento e tracciamento dello spazio.
- Envelope following e implementazione LAR.
- adattività formale (peak follower con contatore).

### Studio del larsen 

- one pole
- comb
- allpass
  

### elementi di sostenibilità

- costruzione di tutte le funzioni base utilizzanto solo i primitivi del linguaggio e diagramma a blocchi di tutto per garantire il porting nei vari linguaggi.
- esempio della patch riportata su max con gen.