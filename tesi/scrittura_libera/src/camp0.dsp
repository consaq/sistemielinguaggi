import("stdfaust.lib");

gain= hslider("gain",50,0,200,1)/200:si.smoo;

// Low Pass One Pole
twopi = 2*ma.PI;
omega(fc) = fc*twopi/ma.SR;
// BI-LINEAR TRANSFORM
aapp(fc) = pow(ma.E, 0-omega(fc));
//
// g -> filter coeff.
lp1p(g) = *(1-g):+~*(g);

// INTEGRATOR
//  average of absolute values of input samples, 
//  over specific time windows (one may try RMS measures, instead)
//
// s -> seconds
integrator(s) = abs : lp1p(aapp(1/s));

// DELAY IN FEEDBACK LOOP
delayFbk(s,fbk) = (+ : de.delay(ma.SR, ba.sec2samp(s)-1))~*(fbk);

mapQ(x) = (1-x)^2;

// valori:
// - fdbk = 0.0001 , 0.94
// - delay at = 0.01
// - integrator ig = 0.01
camp0(at,ig) = _ <: de.delay(ma.SR,ba.sec2samp(at)) * (integrator(ig) : delayFbk(0.0001,0.94) : mapQ);
//camp0(at,ig)=_<:de.delay(ma.SR,ba.sec2samp(at))*(integrator(ig):delayFbk(10,0.98):mapQ);
process = camp0(0.01,0.01);