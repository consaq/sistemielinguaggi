import("stdfaust.lib");
twopi = 2*ma.PI;
omega(fc) = fc*twopi/ma.SR;
aapp(fc) = pow(ma.E, 0-omega(fc));
lp1p(g) = *(1-g):+~*(g);
integrator(s) = abs : lp1p(aapp(1/s));
process = _ : integrator(1);