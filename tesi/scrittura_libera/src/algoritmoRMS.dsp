import("stdfaust.lib");
// Moving Average Envelope Follower
movingAverage(seconds, x) = x - x@(seconds * ma.SR) : 
    fi.pole(1) / (seconds * ma.SR);

// Moving Average RMS
movingAverageRMS(seconds, x) = sqrt(max(0, movingAverage(seconds, x ^ 2)));

process = _ : movingAverageRMS(0.01);
