import("stdfaust.lib");
// Peak Envelope Follower
peakenvelope(period, x) = loop ~ _
    with {
        loop(y) = max(abs(x), y * coeff);
        twoPIforT = (2.0 * ma.PI) * (1.0 / ma.SR);
        coeff = exp(-twoPIforT / max(ma.EPSILON, period));
    };
        
diffTresh(x) = x > x';

line(value, trigger) = (feedf ~ feedb)
 with 
    {   
        break = value;
        y = trigger > trigger';
        feedf(x) = (x > 0) + (x * (x > 0)) + y;
        feedb(x) = (x <= break) * x;
        offset(x) = ((x - 1) > 0) * (x - 1);
    };

// Moving Average RMS
movingAverageRMS(seconds, x) = sqrt(max(0, movingAverage(seconds, x ^ 2)));
//process = movingAverageRMS(1);

// Moving Average Envelope Follower
movingAverage(seconds, x) = x - x@(seconds * ma.SR) : 
    fi.pole(1) / (seconds * ma.SR);
//process = _ * 2 <: movingAverageRMS(.1),_;

time = 0.1;
process = _ <: (peakenvelope(time) > 0.10 : (line(ma.SR*time) / (ma.SR*time)) > 0 : diffTresh);