\section{Envelope Follower}\label{envelope-follower}

Un envelope follower è un algoritmo utilizzato
nell\textquotesingle elaborazione del suono per tracciare
l\textquotesingle andamento dell\textquotesingle ampiezza di un segnale
audio nel tempo. Il suo scopo principale è estrarre e replicare
dinamicamente il volume del segnale audio di ingresso attraverso un
sotto campionamento. L\textquotesingle envelope follower inizia con il
rilevamento dell\textquotesingle ampiezza del segnale audio di ingresso.
Questo può essere realizzato utilizzando un raddrizzatore di precisione
o un circuito di misurazione che cattura i picchi positivi o negativi
del segnale.

\subsection{Tipologie di Algoritmi per gli Envelope
Followers}\label{tipologie-di-algoritmi-per-gli-envelope-followers}

Esistono diverse tipologie di algoritmi per gli \emph{envelope
followers}, ognuno con caratteristiche specifiche e applicazioni ideali.
Ecco alcune delle principali tipologie:

\subsubsection{Peak Envelope Follower}\label{peak-envelope-follower}

Questo algoritmo traccia il picco massimo dell\textquotesingle ampiezza
del segnale entro una finestra temporale. È particolarmente utile per
segnali impulsivi o per preservare le transizioni rapide. 
Calcola il valore assoluto dei campioni del segnale e utilizza un filtro passa-basso per tracciare il picco massimo.

\subsubsection{RMS Envelope Follower}\label{rms-envelope-follower}

Simile al \emph{peak envelope follower}, ma utilizza il valore RMS per
tracciare l\textquotesingle inviluppo. Questo approccio è utile per
ottenere una rappresentazione più fluida e meno sensibile ai picchi
impulsivi rispetto al valore assoluto.

\[
\text{RMS} = \sqrt{\frac{1}{N} \sum_{i=1}^{N} x_i^2}
\]
Dove \(N\) è il numero totale di campioni del segnale e \(x_i\) sono i singoli campioni del segnale.

\subsubsection{Adaptive Envelope
Follower}\label{adaptive-envelope-follower}

Un algoritmo più sofisticato che può adattare la sua risposta in base
alle caratteristiche del segnale. Può regolare dinamicamente i parametri
del filtro in funzione della velocità di variazione del segnale.

Caratteristiche:

\begin{enumerate}
\item
  Maggiore complessità computazionale.
\item
  Migliore risposta a segnali con dinamiche variabili.
\end{enumerate}

\subsubsection{Confronto tra Algoritmi}\label{confronto-tra-algoritmi}

\begin{table}[ht]
  \centering
  \footnotesize
  \renewcommand{\arraystretch}{1.5} % Aumenta la spaziatura tra le righe
  \begin{tabular}{|>{\centering\arraybackslash}p{6cm}|>{\centering\arraybackslash}p{5cm}|>{\centering\arraybackslash}p{5cm}|}
      \hline
      \textbf{Algoritmo} & \textbf{Vantaggi} & \textbf{Svantaggi} \\
      \hline
      Peak Envelope Follower & Preserva le transizioni rapide & Sensibile ai picchi impulsivi \\
      \hline
      RMS Envelope Follower & Risposta più fluida & Maggiore complessità computazionale \\
      \hline
      Adaptive Envelope Follower & Adattivo alle dinamiche del segnale & Complesso e pesante computazionalmente \\
      \hline
  \end{tabular}
  \caption{Vantaggi e svantaggi degli algoritmi di envelope follower}\label{tab:envelope}
\end{table}


Ogni tipo di algoritmo ha i suoi usi specifici e può essere scelto in
base alle esigenze dell' applicazione, alla natura del
segnale e alle risorse computazionali disponibili.

\newpage


\subsection{Algoritmo Peak Envelope}\label{algoritmo-peak-envelope}

\begin{lstlisting}
import("stdfaust.lib");
twopi = 2*ma.PI;
omega(fc) = fc*twopi/ma.SR;
aapp(fc) = pow(ma.E, 0-omega(fc));
lp1p(g) = *(1-g):+~*(g);
integrator(s) = abs : lp1p(aapp(1/s));
process = _ : integrator(1);
\end{lstlisting}



\begin{wrapfigure}{r}{0.5\textwidth}
  \centering
  \includesvg[width=1\linewidth]{docs/camp0-svg/aapp-0x13e62b080.svg}
  \caption{Implementazione trasformata bilineare.}
\end{wrapfigure}


    La funzione \texttt{omega (fc)} calcola la frequenza angolare
    \texttt{omega} per una data frequenza di taglio \texttt{fc},
    utilizzando la formula della trasformata bilineare.


    La funzione \texttt{aapp (fc)} applica la trasformata bilineare
    all\textquotesingle esponenziale negativo della frequenza angolare
    \texttt{omega (fc)}. Questa operazione è cruciale per la
    progettazione di filtri digitali, poiché stabilisce il comportamento
    del filtro nel dominio digitale rispetto alla sua controparte
    analogica. 



    Il filtro \texttt{lp1p (g)} è un filtro passa basso a un polo, dove
    \texttt{g} rappresenta il coefficiente di guadagno del filtro.
    Questo tipo di filtro è progettato per attenuare le alte frequenze
    mantenendo quelle basse.
    \begin{figure}[h]
      \centering
      \includesvg[width=1\linewidth]{docs/camp0-svg/lp1p-0x13e6302e0.svg}
      \caption{Filtro One pole}
    \end{figure}

  L\textquotesingle{}\texttt{integrator (s)} è un componente che
    calcola la media degli valori assoluti dei campioni di ingresso su
    finestre temporali specifiche.

 

    \begin{wrapfigure}{r}{0.5\textwidth}
      \centering
      \includesvg[width=1\linewidth]{docs/camp0-svg/integrator-0x13e630450.svg}
    \end{wrapfigure}  

    Utilizza il filtro passa basso a un polo \texttt{lp1p} con il
    coefficiente \texttt{aap (1/s)}, dove \texttt{s} rappresenta il
    periodo di integrazione espresso in secondi.

\subsection{Algoritmo RMS}\label{algoritmo-rms}

\begin{lstlisting}
import("stdfaust.lib");
// Moving Average Envelope Follower
movingAverage(seconds, x) = x - x@(seconds * ma.SR) : fi.pole(1) / (seconds * ma.SR);
  
// Moving Average RMS
movingAverageRMS(seconds, x) = sqrt(max(0, movingAverage(seconds, x ^ 2)));
  
process = _ : movingAverageRMS(0.01);
\end{lstlisting}

\begin{figure}[h]
  \centering
  \includesvg[width=1\linewidth]{docs/algoritmoRMS-svg/movingAverage-0x141b24e30.svg}
\end{figure}  

La funzione \texttt{movingAverage (seconds,\ x)} calcola una media mobile
dell\textquotesingle ampiezza del segnale \texttt{x} nel tempo\cite{silvisistemi}. Questo
tipo di media mobile è utile per monitorare la variazione
dell\textquotesingle ampiezza nel tempo, filtrando le variazioni a breve
termine e evidenziando tendenze più lunghe nel segnale. In particolare:

\begin{itemize}
\item
  Utilizza un filtro pole ( \texttt{fi.pole 1} ) per applicare la
  media mobile.
\item
  \texttt{seconds} rappresenta il periodo di tempo su cui viene
  calcolata la media mobile, espresso in secondi.
\item
  \texttt{x - x@ (seconds * ma.SR)} calcola la differenza tra il
  segnale originale e il segnale ritardato di \texttt{seconds} secondi.
\item
  \texttt{fi.pole (1) / (seconds * ma.SR)} applica il filtro pole per
  ottenere la media mobile, considerando il tasso di campionamento del
  segnale (\texttt{ma.SR}).

\end{itemize}

\begin{figure}
  \centering
\includesvg[width=1\linewidth]{docs/algoritmoRMS-svg/movingAverageRMS-0x141b26270.svg}
\end{figure}  

La funzione \texttt{movingAverageRMS(seconds, x)} calcola la radice quadrata della
media mobile dell\textquotesingle energia del segnale \texttt{x}. Questo
è un indicatore RMS (Root Mean Square) dell\textquotesingle ampiezza del
segnale nel tempo:

\begin{itemize}
\item
  Utilizza la funzione \texttt{movingAverage} per calcolare la media
  mobile del segnale al quadrato (\texttt{x\ \^{}\ 2}), assicurandosi
  che il risultato sia sempre non negativo con \texttt{max(0,\ ...)}.
\item
  \texttt{sqrt()} calcola la radice quadrata della media mobile,
  fornendo una stima RMS dell\textquotesingle ampiezza del segnale.
\end{itemize}

\subsubsection{Applicazioni e Utilità}\label{applicazioni-e-utilituxe0}

Questo tipo di approccio è particolarmente utile nelle applicazioni di
elaborazione audio in tempo reale per diverse ragioni:

\begin{itemize}
\item
  \textbf{Monitoraggio della Dinamica}: La media mobile permette di
  monitorare la variazione dinamica del segnale audio nel tempo,
  facilitando il controllo del volume o
  l\textquotesingle implementazione di effetti basati
  sull\textquotesingle ampiezza.
\item
  \textbf{Stabilizzazione dell\textquotesingle Analisi}: Calcolare
  l\textquotesingle ampiezza media su un periodo di tempo aiuta a
  stabilizzare l\textquotesingle analisi del segnale, riducendo il
  rumore e le fluttuazioni indesiderate.
\item
  \textbf{Controllo Preciso}: La media mobile RMS fornisce una stima
  accurata dell\textquotesingle energia media del segnale, essenziale
  per applicazioni che richiedono una regolazione precisa della dinamica
  audio.
\end{itemize}

Questo approccio è cruciale per garantire una risposta controllata e
precisa nell\textquotesingle elaborazione del segnale audio, adattabile
a una vasta gamma di applicazioni audio e musicali.

\subsection{Peak Envelope Follower
adattivo}\label{peak-envelope-follower-adattivo}

L\textquotesingle algoritmo presentato è composto da una serie di
funzioni collegate in serie per manipolare e analizzare un segnale audio
in tempo reale. Ecco una spiegazione dettagliata di come funziona questa
composizione: Utilizza un approccio basato su feedback per calcolare
dinamicamente l\textquotesingle inviluppo del segnale, garantendo una
risposta rapida ai picchi e una transizione graduale durante i periodi
di basso livello del segnale.


\begin{lstlisting}
import("stdfaust.lib");
peakenvelope(period, x) = loop ~ _
    with {
        loop(y) = max(abs(x), y * coeff);
        twoPIforT = (2.0 * ma.PI) * (1.0 / ma.SR);
        coeff = exp(-twoPIforT / max(ma.EPSILON, period));
    };
        
line(value, trigger) = (feedf ~ feedb)
 with 
    {   
        break = value;
        y = trigger > trigger';
        feedf(x) = (x > 0) + (x * (x > 0)) + y;
        feedb(x) = (x <= break) * x;
        offset(x) = ((x - 1) > 0) * (x - 1);
    };

diffTresh(x) = x > x';

time = 0.1;

process = _ <: (peakenvelope(time) > 0.10 : 
         (line(ma.SR*time) / (ma.SR*time)) > 0 : 
         diffTresh), _;

\end{lstlisting}

\begin{figure}[h]
  \centering
  \includesvg[width=1\textwidth]{docs/contatorePicchi-svg/process.svg}
\end{figure}  


\subsubsection{Composizione
dell\textquotesingle Algoritmo}\label{composizione-dellalgoritmo}

\begin{wrapfigure}[7]{r}{0.4\textwidth}
  \centering
  \includesvg[width=1\linewidth]{docs/contatorePicchi-svg/peakenvelope-0x13d7ac4e0.svg}
\end{wrapfigure}

  \textbf{Peakenvelope (\texttt{peakenvelope(period,\ x)})}:
    Questa funzione traccia il picco massimo
    dell\textquotesingle ampiezza del segnale \texttt{x} nel tempo
    utilizzando un \emph{Peak Envelope Follower}. Calcola un inviluppo
    che segue i picchi del segnale in ingresso.

    \textbf{Line (\texttt{line(value,\ trigger)})}:

    Questa funzione combina un processo di feedforward (\texttt{feedf})
    e di feedback (\texttt{feedb}) con un parametro di interruzione
    (\texttt{break}). È utilizzato per generare un segnale basato su
    condizioni specifiche di valore e trigger.


%    \begin{figure}[H]
 %     \centering
 %         \includesvg[width=.8\textwidth]{docs/contatorePicchi-svg/feedf-0x13d7b0c90.svg}
 %       \end{figure}

 %   \includesvg{docs/contatorePicchi-svg/feedf-0x13d7b0c90.svg}
 %   \includesvg{docs/contatorePicchi-svg/feedb-0x13d7b2df0.svg}

 \begin{figure}[h]
  \centering
  \begin{subfigure}[b]{0.5\linewidth}
    \includesvg[width=\linewidth]{docs/contatorePicchi-svg/feedf-0x13d7b0c90.svg}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\linewidth}
    \includesvg[width=\linewidth]{docs/contatorePicchi-svg/feedb-0x13d7b2df0.svg}
  \end{subfigure}
\end{figure}

\begin{wrapfigure}[7]{r}{0.5\textwidth}
  \centering
  \includesvg[width=1\linewidth]{docs/contatorePicchi-svg/diffTresh-0x13d7b42f0.svg}
\end{wrapfigure}

  \textbf{DiffTresh}:
  Una funzione semplice che confronta ogni campione del segnale con il
  suo precedente per determinare se è superiore al suo valore
  precedente.


ì
\subsubsection{Codice Peakenvelope}\label{codice-peakenvelope}
\begin{lstlisting}
  peakenvelope(period, x) = loop ~ _
      with {
          loop(y) = max(abs(x), y * coeff);
          twoPIforT = (2.0 * ma.PI) * (1.0 / ma.SR);
          coeff = exp(-twoPIforT / max(ma.EPSILON, period));
      };
\end{lstlisting}  

\subsubsection{Dettagli
dell\textquotesingle Implementazione}\label{dettagli-dellimplementazione}


\begin{figure}[h]
  \centering
  \includesvg[width=1\linewidth]{docs/contatorePicchi-svg/loop-0x13d7ac2f0.svg}
\end{figure}

\textbf{Loop di Feedback}:
    La funzione \texttt{loop(y)} rappresenta il ciclo di feedback
    dell\textquotesingle algoritmo. In ogni iterazione, confronta il
    valore assoluto del segnale corrente \texttt{abs(x)} con il valore
    precedente attenuato \texttt{y\ *\ coeff}, dove \texttt{y} è
    l\textquotesingle inviluppo calcolato in precedenza.

    \begin{wrapfigure}{r}{0.6\textwidth}
      \centering
      \includesvg[width=1\linewidth]{docs/contatorePicchi-svg/coeff-0x13d7ab000.svg}
    \end{wrapfigure}
 
    \textbf{Coefficiente di Smorzamento (coeff)}:
    \texttt{coeff} è un parametro che controlla la velocità di
    decadimento dell\textquotesingle inviluppo. Viene calcolato
    utilizzando il periodo di campionamento del segnale \texttt{period}
    e la costante \texttt{twoPIforT}, che rappresenta $\frac{2pi}{T}$.
    Questo calcolo determina quanto rapidamente \\ l\textquotesingle inviluppo si adatta
    ai cambiamenti nel segnale.

  L\textquotesingle algoritmo inizia calcolando il valore assoluto del
  segnale di ingresso \texttt{x}.
  Utilizza un filtro o una funzione di smoothing per evitare
  oscillazioni indesiderate \\ nell'inviluppo.
  Risponde velocemente ai picchi del segnale, mantenendo
  un\textquotesingle accurata rappresentazione
  dell\textquotesingle ampiezza massima nel tempo.

\subsubsection{Concatenazione delle
Funzioni}\label{concatenazione-delle-funzioni}

Nell\textquotesingle algoritmo la concatenazione delle funzioni ha lo
scopo di isolare il primo campione che supera una soglia specifica e di
escludere i successivi campioni entro un intervallo di tempo di 0.1
secondi. Questo approccio è comune nelle applicazioni di elaborazione
del segnale audio per vari motivi:

\begin{enumerate}
\item
  \textbf{Isolamento del Primo Campione Significativo}:

  \begin{itemize}
  \item
    Utilizzando la funzione \emph{Peakenvelope}, si traccia
    l\textquotesingle inviluppo dell\textquotesingle ampiezza del
    segnale. Quando l\textquotesingle ampiezza supera una determinata
    soglia (\texttt{0.10} nel nostro caso), il segnale passa attraverso
    la condizione \texttt{peakenvelope(time)\ \textgreater{}\ 0.10}.
  \item
    Questo passaggio è cruciale per identificare il momento in cui il
    segnale supera il livello desiderato per la prima volta.
  \end{itemize}
\item
  \textbf{Esclusione dei Campioni Successivi entro 0.1 Secondi}:

  \begin{itemize}
  \item
    Dopo aver rilevato il superamento della soglia, il segnale viene
    processato attraverso la funzione
    \texttt{line (ma.SR*time)/ (ma.SR*time) > 0 : diffTresh}.
  \item
    La funzione \texttt{line (value, trigger)} combina un processo di
    feedforward e di feedback per generare un segnale che tiene conto di
    una specifica soglia di \texttt{value} e di un \texttt{trigger}.
  \item
    Questo passaggio è progettato per escludere i successivi campioni
    che cadono al di sopra della soglia dopo il primo superamento entro
    l\textquotesingle intervallo di tempo specificato (\texttt{0.1}
    secondi).
  \end{itemize}
\item
  \textbf{Applicazioni e Utilità}:

  \begin{itemize}
  \item
    Spesso, l\textquotesingle attacco di un suono può avere più campioni
    sopra una certa soglia, e isolare solo il primo campione
    significativo aiuta a evitare artefatti indesiderati come
    \emph{ripples} nell\textquotesingle inviluppo dei picchi
    (\emph{peakenvelope}) quando la curva inizia a discendere.
  \item
    La funzione \texttt{diffTresh} si attiva quindi una sola volta per
    ogni picco significativo, garantendo una risposta più stabile e
    precisa nell\textquotesingle analisi e
    nell\textquotesingle elaborazione del segnale audio.
  \end{itemize}
\end{enumerate}

\subsection{Envelope follower
coniugato}\label{envelope-follower-coniugato}

Il codice FAUST qui presentato è progettato per rilevare il picco di un
segnale audio e, utilizzando un ritardo con feedback, crea un effetto di
smussamento che consente di avere una curva di rilascio di durata
variabile. Ecco una spiegazione dettagliata di come funziona questo
processo: 

\begin{lstlisting}

  import("stdfaust.lib");

twopi = 2*ma.PI;
omega(fc) = fc*twopi/ma.SR;
aapp(fc) = pow(ma.E, 0-omega(fc));
lp1p(g) = *(1-g):+~*(g);
integrator(s) = abs : lp1p(aapp(1/s));

delayFbk(s,fbk) = (+ : de.delay(ma.SR, ba.sec2samp(s)-1))~*(fbk);

mapQ(x) = (1-x)^2;

camp0(at,ig) = _ <: de.delay(ma.SR,ba.sec2samp(at)) * (integrator(ig) : 
                     delayFbk(0.0001,0.94) : mapQ);

process = camp0(0.01,0.01);

\end{lstlisting}

\begin{figure}[t]
  \centering
  \includesvg[width=1\linewidth]{docs/camp0-svg/process.svg}
\end{figure}


Il modulo integratore
è stato presentato recentemente.
Il modulo \texttt{delayFbk} introduce un ritardo con feedback nel
segnale. Il ritardo è specificato in secondi (s), e \texttt{fbk}
rappresenta il fattore di feedback. Il feedback riporta una parte del
segnale ritardato all\textquotesingle ingresso, creando un effetto di
smussamento. 

\begin{figure}
  \centering
\includesvg[width=.8\linewidth]{docs/camp0-svg/delayFbk-0x13e63c520.svg}
\end{figure}

