\section{Tutto è un filtro}\label{tutto-uxe8-un-filtro}

Ogni sistema che riceve un segnale in ingresso e ne elabora un altro in
uscita è un filtro. La comprensione di questi oggetti nel dominio
digitale potrebbe risultare superficiale se non si comprende la
provenienza della loro natura analogica. Partendo dal controllo in
voltaggio, procederemo verso la teorizzazione digitale dei filtri.

\subsection{Da Analogico a Virtuale}\label{da-analogico-a-virtuale}

Nella breve spiegazione di questo concetto molto importante, è bene
illustrare la motivazione fondamentale del VA (virtual analog): il
dominio digitale è discreto, quindi soggetto a una divisione in istanti
temporali minimi uguali tra loro (periodo di campionamento). I
\emph{samples} (campioni) sono valori numerici quantizzati (in un numero
di bit pari alla sensibilità di quantizzazione). Il numero di
\emph{samples} al secondo varia in funzione della frequenza di
campionamento. Utilizzando l\textquotesingle equazione che unisce il
periodo alla frequenza, ovvero

\begin{equation}
T = \frac{1}{f}
\end{equation}

svolgiamo la prima operazione in FAUST per analizzare il valore del
periodo di campionamento in base alla frequenza di campionamento della
macchina:

\begin{lstlisting}
import("stdfaust.lib"); 
T= 1/ma.SR;   // T= periodo di campionamento 
process = T;
\end{lstlisting}


Supponendo che abbiamo settato la nostra macchina a una frequenza di
campionamento di $96 kHz$, il risultato di questa equazione sarà:
$0,0000104...$ secondi. Sembrerebbe un tempo molto breve, ma non è
paragonabile al rapporto con la corrente elettrica, che viaggia alla
velocità della luce e ha un tempo infinitamente piccolo al punto da
essere trascurabile.

\begin{wrapfigure}{r}{0.4\textwidth}
  \centering
  \includesvg[width=0.9\linewidth]{docs/Low_pass_filter.svg}
  \caption{Topologia ideale filtro $RC$.}
\end{wrapfigure}


Un filtro analogico utilizza componenti come resistenze, condensatori e
talvolta induttori per alterare le caratteristiche del segnale in
ingresso. Ad esempio, un semplice filtro passa-basso può essere
costruito con una resistenza e un condensatore, dove la resistenza
limita il flusso di corrente e il condensatore accumula e rilascia
carica elettrica, smorzando le alte frequenze del segnale. Un esempio è mostrato nella Figura 1.

Nel contesto analogico, la risposta del filtro è continua e può reagire
a variazioni infinitesimali nel segnale. La risposta in frequenza di un
filtro RC passa-basso è determinata dalla formula:

\begin{equation}
H(f) = \frac{1}{1 + j2pi fRC}
\end{equation} 


dove $R$ è la resistenza, $C$ è la capacità e $f$ è la frequenza
del segnale. Questo significa che a frequenze basse, la reattanza del
condensatore è alta e quindi il segnale passa attraverso il filtro. A
frequenze alte, la reattanza è bassa e il segnale viene attenuato.

La conversione di questi concetti in un contesto digitale richiede la
discretizzazione del segnale e l\textquotesingle utilizzo di algoritmi
che emulano il comportamento dei componenti analogici\cite{zavalishin2008preserving}. In FAUST,
possiamo modellare un filtro passa-basso semplice utilizzando
l\textquotesingle approssimazione della trasformata bilineare per
ottenere una rappresentazione digitale accurata del filtro analogico.

La teorizzazione dei filtri da analogici a digitali coinvolge diversi
passaggi fondamentali che permettono di trasformare un filtro progettato
nel dominio analogico in un filtro equivalente nel dominio digitale.
Potremmo formalizzare i passaggi principali:

\begin{enumerate}
\item
  \textbf{Specifiche del filtro analogico}: Inizialmente si definiscono
  le specifiche del filtro desiderato nel dominio analogico. Queste
  specifiche includono la risposta in frequenza desiderata (come banda
  passante, banda stop, attenuazione e guadagno) e altre caratteristiche
  come la pendenza della transizione tra le bande.
\item
  \textbf{Trasformazione bilineare o invariante al tempo}: Una delle
  tecniche più comuni per convertire un filtro analogico in uno digitale
  è la trasformazione bilineare (o invariante al tempo). Questa
  trasformazione mappa il piano complesso del filtro analogico (in
  frequenza) nel piano complesso del filtro digitale. In pratica, questa
  trasformazione preserva la risposta in frequenza e le caratteristiche
  di fase del filtro analogico convertendole in un filtro digitale.
\item
  \textbf{Discretizzazione dei coefficienti}: Una volta ottenuto il
  filtro digitale attraverso la trasformazione, è necessario
  discretizzare i coefficienti del filtro. Nel dominio digitale, i
  coefficienti che definiscono come il filtro combinare i campioni di
  input sono numeri finiti. Questi coefficienti devono essere calcolati
  o approssimati in base ai requisiti di progettazione del filtro
  digitale.
\item
  \textbf{Simulazione e verifica}: Dopo la progettazione, è essenziale
  simulare il filtro digitale per verificare che le specifiche
  desiderate siano soddisfatte. Questo passo include
  l\textquotesingle analisi della risposta in frequenza, della risposta
  impulsiva e di altre caratteristiche del filtro per assicurarsi che il
  filtro digitale comporti correttamente nel dominio digitale ciò che ci
  si aspettava dal filtro analogico originale.
\item
  \textbf{Implementazione}: Infine, una volta che il filtro digitale è
  stato progettato e verificato, può essere implementato in software
  (come un algoritmo di elaborazione digitale del segnale) per essere
  utilizzato nel sistema desiderato.
\end{enumerate}

La trasformata bilineare, talvolta nota anche come trasformazione
bilineare di Zavalishin, è una tecnica utilizzata per convertire un
filtro analogico in un filtro digitale nel dominio delle frequenze.
Questa trasformazione è ampiamente impiegata nell\textquotesingle ambito
dell\textquotesingle elaborazione digitale dei segnali per preservare la
risposta in frequenza del filtro analogico nel filtro digitale
corrispondente.

\subsubsection{Principi di Base della Trasformata
Bilineare}\label{principi-di-base-della-trasformata-bilineare}

La trasformata bilineare opera sostituendo la variabile complessa $s$
(usata nel dominio analogico) con la variabile complessa $z$ (usata
nel dominio digitale). Il passaggio chiave è la mappatura delle
frequenze dal dominio analogico al dominio digitale, preservando la
risposta in frequenza del filtro. La trasformazione avviene secondo la
seguente relazione:

\begin{equation}
  s = \frac{2}{T} \cdot \frac{1 - z^{-1}}{1 + z^{-1}}
  \end{equation}
  
dove $T$ è l'intervallo di campionamento in secondi.
Questa equazione trasforma un filtro analogico con variabile complessa $s$ in un filtro digitale con variabile complessa $z$.

\subsubsection{Proprietà e
Caratteristiche}\label{proprietuxe0-e-caratteristiche}

\begin{enumerate}
\item
  \textbf{Preservazione della Risposta in Frequenza}: La trasformata
  bilineare preserva la risposta in frequenza del filtro analogico nel
  filtro digitale corrispondente. Questo è essenziale per garantire che
  le caratteristiche di frequenza desiderate, come la banda passante e
  la banda stop, siano mantenute nel filtro digitale.
\item
  \textbf{Distorsione delle Frequenze Elevate}:
  Un\textquotesingle inconveniente della trasformazione bilineare è che
  le frequenze elevate nel filtro analogico vengono distorte nel filtro
  digitale. Questo è dovuto alla natura non lineare della
  trasformazione, che comprime le frequenze alte nel dominio digitale.
\item
  \textbf{Equivalenza in Frequenza}: Nonostante la distorsione delle
  frequenze elevate, la trasformata bilineare garantisce che le
  frequenze normali (non troppo alte) siano rappresentate in modo
  equivalente tra il filtro analogico e quello digitale. Questo è
  fondamentale per mantenere la coerenza nelle caratteristiche di
  risposta del filtro.
\end{enumerate}

In sintesi, la trasformata bilineare di Zavalishin è una tecnica
cruciale per convertire filtri analogici in filtri digitali mantenendo
la risposta in frequenza, sebbene sia necessario considerare le sue
limitazioni, come la distorsione delle frequenze elevate, durante il
processo di progettazione e implementazione dei filtri digitali.

\subsection{Filtri}\label{filtri}

L\textquotesingle ordine di un filtro digitale è una misura della
complessità del filtro stesso ed è definito come il numero di campioni
di input (o coefficienti) su cui il filtro si basa per produrre
un\textquotesingle uscita. Più formalmente, l\textquotesingle ordine di
un filtro digitale può essere descritto nei seguenti modi:


\textbf{Filtro FIR (Finite Impulse Response):} L\textquotesingle ordine
di un filtro FIR è il numero di campioni di input meno uno che il filtro
utilizza. In altre parole, se un filtro FIR utilizza $N$ campioni di
input per calcolare ogni campione di output, l\textquotesingle ordine
del filtro è $N - 1$.

La formula generale di un filtro FIR di ordine $N$ è:

\begin{equation}
  y[n] = b_0 x[n] + b_1 x[n-1] + b_2 x[n-2] + \ldots + b_N x[n-N]
  \end{equation}
  
dove $y[n]$ è il campione di output, $x[n]$ è il campione di
input e $b$ $i$ sono i coefficienti del filtro.

Ci sono diverse ragioni per cui un filtro FIR (Finite Impulse Response)
non può essere analogico ma è specificamente implementato come filtro
digitale. Ecco le principali motivazioni:

\begin{enumerate}
\item
  \textbf{Impulso Finito di Risposta (FIR)}: Un filtro FIR è
  caratterizzato dal fatto che la sua risposta
  all\textquotesingle impulso è di durata finita. Questo significa che
  l\textquotesingle uscita del filtro dipende solo da una combinazione
  finita di campioni di input passati e presenti. In un dominio
  analogico, un\textquotesingle impulso di durata finita è impossibile
  da realizzare, poiché un segnale analogico ha una durata infinita nel
  tempo.
\item
  \textbf{Coefficienti Discreti}: Un filtro FIR opera utilizzando
  coefficienti discreti (o pesi) applicati a ciascun campione di input.
  Questi coefficienti determinano come i campioni di input vengono
  combinati per produrre l\textquotesingle uscita. Nel dominio
  analogico, non esiste un concetto diretto di coefficienti discreti; i
  pesi di un filtro analogico sarebbero continui e non discreti come nel
  caso digitale.
\item
  \textbf{Implementazione Pratica}: Anche se esiste la teoria dei filtri
  FIR analogici, implementarli in pratica è estremamente difficile o
  impraticabile. La manipolazione precisa dei pesi (che in un filtro
  digitale sono rappresentati come numeri finiti) richiederebbe
  componenti analogici con caratteristiche estremamente precise e
  costanti nel tempo, cosa che è difficile da ottenere a causa delle
  variazioni e delle instabilità tipiche dei componenti analogici.
\item
  \textbf{Precisione e Controllo}: I filtri digitali offrono un
  controllo e una precisione molto maggiori rispetto ai loro equivalenti
  analogici. La precisione numerica delle operazioni digitali consente
  di progettare filtri con specifiche esatte e riproducibili, cosa che
  sarebbe difficile o impossibile con un filtro analogico a causa di
  fenomeni come il rumore, la deriva termica e altre imperfezioni dei
  componenti fisici.
\end{enumerate}

\begin{wrapfigure}{r}{0.3\textwidth}
  \centering
  \includesvg[width=1\linewidth]{docs/oneZero.svg}
  \caption{FIR di primo
  ordine con campione di ritardo senza coefficienti.}
\end{wrapfigure}

In sintesi, il concetto di filtro FIR è strettamente legato
all\textquotesingle elaborazione digitale del segnale, poiché sfrutta le
proprietà discrete del campionamento e della rappresentazione numerica.
Questo rende il filtro FIR una scelta naturale e efficace per
l\textquotesingle implementazione in sistemi digitali, mentre
l\textquotesingle implementazione di un filtro analogico con risposta
all\textquotesingle impulso finita risulta praticamente inattuabile.

\textbf{Filtro IIR (Infinite Impulse Response):}
L\textquotesingle ordine di un filtro IIR è il massimo tra il numero di
termini del feedback (campioni passati dell\textquotesingle uscita) e il
numero di termini del feedforward (campioni passati
dell\textquotesingle input) meno uno.

La formula generale di un filtro IIR di ordine $N$ è:

\begin{equation}
  y[n] = \frac{\sum_{i=0}^{M} b_i x[n-i]}{\sum_{j=0}^{N} a_j y[n-j]}
  \end{equation}
  
  \newpage

  
  \begin{wrapfigure}{r}{0.3\textwidth}
    \centering
    \includesvg[width=1\linewidth]{docs/integrator.svg}
    \caption{IIR di primo
    ordine con campione di ritardo senza coefficienti.}
  \end{wrapfigure}

  dove \( y[n] \) è l'uscita del sistema al tempo \( n \), \( x[n] \) è l'input del sistema al tempo \( n \), \( b_i \) sono i coefficienti del numeratore della somma, \( a_j \) sono i coefficienti del denominatore della somma, \( M \) e \( N \) sono i rispettivi limiti superiori delle somme.

sono rispettivamente i coefficienti del numeratore e del denominatore
del filtro.


L'ordine del filtro è direttamente collegato alla
risposta in frequenza e alla capacità del filtro di attenuare o
amplificare determinate frequenze. Filtri di ordine più alto possono
avere transizioni più ripide tra le bande di passaggio e le bande di
stop, ma possono anche introdurre una maggiore complessità
computazionale e potenzialmente una maggiore instabilità (specialmente
nei filtri IIR).

\subsection{Rapporto tra Frequenza di Campionamento e Tempo di
Integrazione}\label{rapporto-tra-frequenza-di-campionamento-e-tempo-di-integrazione}




Nella trasformata bilineare, il rapporto tra la frequenza di
campionamento \( f_s \) e il tempo di integrazione \( T_i \) è un
aspetto cruciale che influenza sia il comportamento del filtro nel
dominio del tempo che nel dominio delle frequenze.

\begin{enumerate}
\item
  \textbf{Nel Dominio del Tempo (Integrazione)}:
  \begin{itemize}
  \item
    Il tempo di integrazione \( T_i \) determina quanto a lungo il filtro considera i campioni di input
    prima di produrre un output. Un tempo di integrazione più lungo
    implica un filtro con una risposta più lenta ai cambiamenti nel
    segnale di ingresso, perché integra su un periodo maggiore di tempo.
    
  \item
    La relazione tra \( T_i \) e \( f_s \) è inversa: \( T_i = \frac{1}{f_s} \). 
    Questo significa che all'aumentare della frequenza di campionamento \( f_s \), il tempo di integrazione \( T_i \) diminuisce e viceversa.
  \end{itemize}

\item
  \textbf{Nel Dominio delle Frequenze (Filtro)}:

  \begin{itemize}
  \item
    Dal punto di vista della frequenza, il tempo di integrazione \( T_i \) è correlato alla larghezza di banda del filtro digitale
    risultante. Un tempo di integrazione più lungo corrisponde a un
    filtro con una banda passante più stretta nel dominio delle
    frequenze, perché il filtro tende a integrare su un intervallo
    temporale più ampio di campioni.
  \item
    In altre parole, un tempo di integrazione più lungo \( T_i \)
    corrisponde a una risposta in frequenza più selettiva e viceversa.
  \end{itemize}
\end{enumerate}

\begin{figure}[htbp]
  \centering
  \includesvg[scale=0.8]{docs/mathematica1.svg}
  \caption{Integrazione}
\end{figure}

Il rapporto tra la frequenza di campionamento \( f_s \) e il tempo di
integrazione \( T_i \) nella trasformata bilineare è cruciale per
determinare sia il comportamento del filtro nel dominio del tempo (come
integratore) che nel dominio delle frequenze (come filtro passa-basso,
passa-alto, ecc.). Questo rapporto definisce la larghezza di banda e la
selettività del filtro digitale risultante, influenzando
significativamente la sua applicazione pratica nella progettazione di
sistemi di elaborazione del segnale digitale.

