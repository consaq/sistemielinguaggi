# Live Electronics atteggiamento ecosistemico

# Abstract
Il focus del lavoro consiste nel costruire le competenze tecnologiche teoriche e pratiche in un ambito del Signal Information Processing (SIP). In particolare è stato affrontato lo studio dei filtri come strumenti tecnici e compositivi per la manipolazione del segnale digitale. Attraverso questi si giunge alla costruzione di funzioni più complesse come descrittori temporali.

# Introduzione
La tesina è costruita dal semplice al complesso, passando per i ragionamenti principali che hanno portato alla costruzione e all'implementazione dei descrittori temporali. Quindi, di seguito avremo filtri e costruzione di envelope followers adattivi per la campana. L'ambiente di sviluppo scelto per l'analisi del segnale digitale in tempo reale è *FAUST* per i seguenti motivi:

1. **Programmare in *FAUST* equivale a scrivere in C++**. È un linguaggio ad alto livello che permette un controllo sul campione a basso livello (ovvero agevola tutte le operazioni complesse per i segnali ritardati al singolo campione come le retroazioni). L'output del *DSP* è compilato in C++.

2. È un linguaggio *OPEN SOURCE*, quindi **NON è un linguaggio proprietario**.

3. Garantisce i *wrappers* per la **sostenibilità multipiattaforma**.

4. L'IDE permette la visualizzazione dei dati nelle seguenti modalità: continua, *offline*, manuale e al singolo evento. Questo permette di fare analisi con visualizzazione su tabelle esportabili in HTML, visualizzare su oscilloscopio il segnale audio e subaudio osservandolo zoomando fino al singolo campione.

5. Si può scegliere una compilazione in *single, double, quad precision*, cosa non possibile negli altri ambienti di sviluppo e fondamentale per molte pratiche esecutive di live electronics ecosistemiche, dove si costruiscono Sistemi Lineari Tempo Varianti.

# Tutto è un filtro
Ogni sistema che riceve un segnale in ingresso e ne elabora un altro in uscita è un filtro. La comprensione di questi oggetti nel dominio digitale potrebbe risultare superficiale se non si comprende la provenienza della loro natura analogica. Partendo dal controllo in voltaggio, procederemo verso la teorizzazione digitale dei filtri.

## Da Analogico a Virtuale
Nella breve spiegazione di questo concetto molto importante, è bene illustrare la motivazione fondamentale del VA (virtual analog): il dominio digitale è discreto, quindi soggetto a una divisione in istanti temporali minimi uguali tra loro (periodo di campionamento). I *samples* (campioni) sono valori numerici quantizzati (in un numero di bit pari alla sensibilità di quantizzazione). Il numero di *samples* al secondo varia in funzione della frequenza di campionamento. Utilizzando l'equazione che unisce il periodo alla frequenza, ovvero

$$
T = \frac{1}{f}
$$

svolgiamo la prima operazione in FAUST per analizzare il valore del periodo di campionamento in base alla frequenza di campionamento della macchina:

```faust
import("stdfaust.lib");
// T = periodo di campionamento
T = 1/ma.SR;
process = T;
```

Supponendo che abbiamo settato la nostra macchina a una frequenza di campionamento di 96 kHz, il risultato di questa equazione sarà: 0,0000104... secondi. Sembrerebbe un tempo molto breve, ma non è paragonabile al rapporto con la corrente elettrica, che viaggia alla velocità della luce e ha un tempo infinitamente piccolo al punto da essere trascurabile.

Un filtro analogico utilizza componenti come resistenze, condensatori e talvolta induttori per alterare le caratteristiche del segnale in ingresso. Ad esempio, un semplice filtro passa-basso può essere costruito con una resistenza e un condensatore, dove la resistenza limita il flusso di corrente e il condensatore accumula e rilascia carica elettrica, smorzando le alte frequenze del segnale.

Nel contesto analogico, la risposta del filtro è continua e può reagire a variazioni infinitesimali nel segnale. La risposta in frequenza di un filtro RC passa-basso è determinata dalla formula:

$$
H(f) = \frac{1}{1 + j2\pi fRC}
$$

dove \( R \) è la resistenza, \( C \) è la capacità e \( f \) è la frequenza del segnale. Questo significa che a frequenze basse, la reattanza del condensatore è alta e quindi il segnale passa attraverso il filtro. A frequenze alte, la reattanza è bassa e il segnale viene attenuato.

![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/Low_pass_filter.svg?ref_type=heads](docs/Low_pass_filter.svg)

La conversione di questi concetti in un contesto digitale richiede la discretizzazione del segnale e l'utilizzo di algoritmi che emulano il comportamento dei componenti analogici. In FAUST, possiamo modellare un filtro passa-basso semplice utilizzando l'approssimazione della trasformata bilineare per ottenere una rappresentazione digitale accurata del filtro analogico.



La teorizzazione dei filtri da analogici a digitali coinvolge diversi passaggi fondamentali che permettono di trasformare un filtro progettato nel dominio analogico in un filtro equivalente nel dominio digitale. Potremmo formalizzare i passaggi principali:

1. **Specifiche del filtro analogico**: Inizialmente si definiscono le specifiche del filtro desiderato nel dominio analogico. Queste specifiche includono la risposta in frequenza desiderata (come banda passante, banda stop, attenuazione e guadagno) e altre caratteristiche come la pendenza della transizione tra le bande.

2. **Trasformazione bilineare o invariante al tempo**: Una delle tecniche più comuni per convertire un filtro analogico in uno digitale è la trasformazione bilineare (o invariante al tempo). Questa trasformazione mappa il piano complesso del filtro analogico (in frequenza) nel piano complesso del filtro digitale. In pratica, questa trasformazione preserva la risposta in frequenza e le caratteristiche di fase del filtro analogico convertendole in un filtro digitale.

3. **Discretizzazione dei coefficienti**: Una volta ottenuto il filtro digitale attraverso la trasformazione, è necessario discretizzare i coefficienti del filtro. Nel dominio digitale, i coefficienti che definiscono come il filtro combinare i campioni di input sono numeri finiti. Questi coefficienti devono essere calcolati o approssimati in base ai requisiti di progettazione del filtro digitale.

4. **Simulazione e verifica**: Dopo la progettazione, è essenziale simulare il filtro digitale per verificare che le specifiche desiderate siano soddisfatte. Questo passo include l'analisi della risposta in frequenza, della risposta impulsiva e di altre caratteristiche del filtro per assicurarsi che il filtro digitale comporti correttamente nel dominio digitale ciò che ci si aspettava dal filtro analogico originale.

5. **Implementazione**: Infine, una volta che il filtro digitale è stato progettato e verificato, può essere implementato in software (come un algoritmo di elaborazione digitale del segnale) per essere utilizzato nel sistema desiderato.

La trasformata bilineare, talvolta nota anche come trasformazione bilineare di Zavalishin, è una tecnica utilizzata per convertire un filtro analogico in un filtro digitale nel dominio delle frequenze. Questa trasformazione è ampiamente impiegata nell'ambito dell'elaborazione digitale dei segnali per preservare la risposta in frequenza del filtro analogico nel filtro digitale corrispondente.

### Principi di Base della Trasformata Bilineare

La trasformata bilineare opera sostituendo la variabile complessa \( s \) (usata nel dominio analogico) con la variabile complessa \( z \) (usata nel dominio digitale). Il passaggio chiave è la mappatura delle frequenze dal dominio analogico al dominio digitale, preservando la risposta in frequenza del filtro. La trasformazione avviene secondo la seguente relazione:

\[ s = \frac{2}{T} \cdot \frac{1 - z^{-1}}{1 + z^{-1}} \]

dove \( T \) è l'intervallo di campionamento in secondi. Questa equazione trasforma un filtro analogico con variabile complessa \( s \) in un filtro digitale con variabile complessa \( z \).

### Proprietà e Caratteristiche

1. **Preservazione della Risposta in Frequenza**: La trasformata bilineare preserva la risposta in frequenza del filtro analogico nel filtro digitale corrispondente. Questo è essenziale per garantire che le caratteristiche di frequenza desiderate, come la banda passante e la banda stop, siano mantenute nel filtro digitale.

2. **Distorsione delle Frequenze Elevate**: Un'inconveniente della trasformazione bilineare è che le frequenze elevate nel filtro analogico vengono distorte nel filtro digitale. Questo è dovuto alla natura non lineare della trasformazione, che comprime le frequenze alte nel dominio digitale.

3. **Equivalenza in Frequenza**: Nonostante la distorsione delle frequenze elevate, la trasformata bilineare garantisce che le frequenze normali (non troppo alte) siano rappresentate in modo equivalente tra il filtro analogico e quello digitale. Questo è fondamentale per mantenere la coerenza nelle caratteristiche di risposta del filtro.

In sintesi, la trasformata bilineare di Zavalishin è una tecnica cruciale per convertire filtri analogici in filtri digitali mantenendo la risposta in frequenza, sebbene sia necessario considerare le sue limitazioni, come la distorsione delle frequenze elevate, durante il processo di progettazione e implementazione dei filtri digitali.


## Filtri 
L'ordine di un filtro digitale è una misura della complessità del filtro stesso ed è definito come il numero di campioni di input (o coefficienti) su cui il filtro si basa per produrre un'uscita. Più formalmente, l'ordine di un filtro digitale può essere descritto nei seguenti modi:

**Filtro FIR (Finite Impulse Response):**
L'ordine di un filtro FIR è il numero di campioni di input meno uno che il filtro utilizza. In altre parole, se un filtro FIR utilizza $N$ campioni di input per calcolare ogni campione di output, l'ordine del filtro è $N - 1$.

La formula generale di un filtro FIR di ordine $N$ è:

$$
y[n] = b_0 x[n] + b_1 x[n-1] + b_2 x[n-2] + \ldots + b_N x[n-N]
$$

dove $y[n]$ è il campione di output, 
$x[n]$ è il campione di input e $b$ $i$ sono i coefficienti del filtro.
​
​![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/oneZero.svg?ref_type=heads](docs/oneZero.svg) 
fig. 2 filtro FIR di primo ordine con campione di ritardo senza coefficienti.

Ci sono diverse ragioni per cui un filtro FIR (Finite Impulse Response) non può essere analogico ma è specificamente implementato come filtro digitale. Ecco le principali motivazioni:

1. **Impulso Finito di Risposta (FIR)**: Un filtro FIR è caratterizzato dal fatto che la sua risposta all'impulso è di durata finita. Questo significa che l'uscita del filtro dipende solo da una combinazione finita di campioni di input passati e presenti. In un dominio analogico, un'impulso di durata finita è impossibile da realizzare, poiché un segnale analogico ha una durata infinita nel tempo.

2. **Coefficienti Discreti**: Un filtro FIR opera utilizzando coefficienti discreti (o pesi) applicati a ciascun campione di input. Questi coefficienti determinano come i campioni di input vengono combinati per produrre l'uscita. Nel dominio analogico, non esiste un concetto diretto di coefficienti discreti; i pesi di un filtro analogico sarebbero continui e non discreti come nel caso digitale.

3. **Implementazione Pratica**: Anche se esiste la teoria dei filtri FIR analogici, implementarli in pratica è estremamente difficile o impraticabile. La manipolazione precisa dei pesi (che in un filtro digitale sono rappresentati come numeri finiti) richiederebbe componenti analogici con caratteristiche estremamente precise e costanti nel tempo, cosa che è difficile da ottenere a causa delle variazioni e delle instabilità tipiche dei componenti analogici.

4. **Precisione e Controllo**: I filtri digitali offrono un controllo e una precisione molto maggiori rispetto ai loro equivalenti analogici. La precisione numerica delle operazioni digitali consente di progettare filtri con specifiche esatte e riproducibili, cosa che sarebbe difficile o impossibile con un filtro analogico a causa di fenomeni come il rumore, la deriva termica e altre imperfezioni dei componenti fisici.

In sintesi, il concetto di filtro FIR è strettamente legato all'elaborazione digitale del segnale, poiché sfrutta le proprietà discrete del campionamento e della rappresentazione numerica. Questo rende il filtro FIR una scelta naturale e efficace per l'implementazione in sistemi digitali, mentre l'implementazione di un filtro analogico con risposta all'impulso finita risulta praticamente inattuabile.


**Filtro IIR (Infinite Impulse Response):**
L'ordine di un filtro IIR è il massimo tra il numero di termini del feedback (campioni passati dell'uscita) e il numero di termini del feedforward (campioni passati dell'input) meno uno.

La formula generale di un filtro IIR di ordine $N$ è:

$$
y[n] = \frac{\sum_{i=0}^{M} b_i x[n-i]}{\sum_{j=0}^{N} a_j y[n-j]}
$$

sono rispettivamente i coefficienti del numeratore e del denominatore del filtro.

![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/simpleFilter.svg](docs/integrator.svg)
fig.1 filtro IIR di primo ordine con un campione di ritardo senza normalizzazione dei coefficienti.



L'ordine del filtro è direttamente collegato alla risposta in frequenza e alla capacità del filtro di attenuare o amplificare determinate frequenze. Filtri di ordine più alto possono avere transizioni più ripide tra le bande di passaggio e le bande di stop, ma possono anche introdurre una maggiore complessità computazionale e potenzialmente una maggiore instabilità (specialmente nei filtri IIR).



## Rapporto tra Frequenza di Campionamento e Tempo di Integrazione
Nella trasformata bilineare, il rapporto tra la frequenza di campionamento \( f_s \) e il tempo di integrazione \( T_i \) è un aspetto cruciale che influenza sia il comportamento del filtro nel dominio del tempo che nel dominio delle frequenze.

1. **Nel Dominio del Tempo (Integrazione)**:
   - Il tempo di integrazione \( T_i \) determina quanto a lungo il filtro considera i campioni di input prima di produrre un output. Un tempo di integrazione più lungo implica un filtro con una risposta più lenta ai cambiamenti nel segnale di ingresso, perché integra su un periodo maggiore di tempo.
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/Low_pass_filter.svg?ref_type=heads](docs/mathematica1.svg)
   - La relazione tra \( T_i \) e \( f_s \) è inversa: \( T_i = \frac{1}{f_s} \). Questo significa che all'aumentare della frequenza di campionamento \( f_s \), il tempo di integrazione \( T_i \) diminuisce e viceversa.

2. **Nel Dominio delle Frequenze (Filtro)**:
   - Dal punto di vista della frequenza, il tempo di integrazione \( T_i \) è correlato alla larghezza di banda del filtro digitale risultante. Un tempo di integrazione più lungo corrisponde a un filtro con una banda passante più stretta nel dominio delle frequenze, perché il filtro tende a integrare su un intervallo temporale più ampio di campioni.
   - In altre parole, un tempo di integrazione più lungo \( T_i \) corrisponde a una risposta in frequenza più selettiva e viceversa.

Il rapporto tra la frequenza di campionamento \( f_s \) e il tempo di integrazione \( T_i \) nella trasformata bilineare è cruciale per determinare sia il comportamento del filtro nel dominio del tempo (come integratore) che nel dominio delle frequenze (come filtro passa-basso, passa-alto, ecc.). Questo rapporto definisce la larghezza di banda e la selettività del filtro digitale risultante, influenzando significativamente la sua applicazione pratica nella progettazione di sistemi di elaborazione del segnale digitale.

# Envelope Follower

Un envelope follower è un algoritmo utilizzato nell'elaborazione del suono per tracciare l'andamento dell'ampiezza di un segnale audio nel tempo. Il suo scopo principale è estrarre e replicare dinamicamente il volume del segnale audio di ingresso attraverso un sotto campionamento.
L'envelope follower inizia con il rilevamento dell'ampiezza del segnale audio di ingresso. Questo può essere realizzato utilizzando un raddrizzatore di precisione o un circuito di misurazione che cattura i picchi positivi o negativi del segnale.

## Tipologie di Algoritmi per gli Envelope Followers

Esistono diverse tipologie di algoritmi per gli *envelope followers*, ognuno con caratteristiche specifiche e applicazioni ideali. Ecco alcune delle principali tipologie:

### Peak Envelope Follower
Questo algoritmo traccia il picco massimo dell'ampiezza del segnale entro una finestra temporale. È particolarmente utile per segnali impulsivi o per preservare le transizioni rapide.

#### Funzionamento
1. Calcola il valore assoluto dei campioni del segnale.
2. Utilizza un filtro passa-basso per tracciare il picco massimo.

### RMS Envelope Follower
Simile al *peak envelope follower*, ma utilizza il valore RMS per tracciare l'inviluppo. Questo approccio è utile per ottenere una rappresentazione più fluida e meno sensibile ai picchi impulsivi rispetto al valore assoluto.

$$
\text{RMS} = \sqrt{\frac{1}{N} \sum_{i=1}^{N} x_i^2}
$$
dove \( x_i \) sono i campioni del segnale e \( N \) è il numero di campioni nella finestra.

### Adaptive Envelope Follower
Un algoritmo più sofisticato che può adattare la sua risposta in base alle caratteristiche del segnale. Può regolare dinamicamente i parametri del filtro in funzione della velocità di variazione del segnale.

#### Caratteristiche
1. Maggiore complessità computazionale.
2. Migliore risposta a segnali con dinamiche variabili.

### Confronto tra Algoritmi
| Algoritmo                     | Vantaggi                                              | Svantaggi                                         |
|-------------------------------|-------------------------------------------------------|--------------------------------------------------|
| Peak Envelope Follower        | Preserva le transizioni rapide                        | Sensibile ai picchi impulsivi                    |
| RMS Envelope Follower         | Risposta più fluida                                   | Maggiore complessità computazionale              |
| Adaptive Envelope Follower    | Adattivo alle dinamiche del segnale                   | Complesso e pesante computazionalmente           |

Ogni tipo di algoritmo ha i suoi usi specifici e può essere scelto in base alle esigenze dell'applicazione, alla natura del segnale e alle risorse computazionali disponibili.

## Algoritmo Peak Envelope


```faust
import("stdfaust.lib");
twopi = 2*ma.PI;
omega(fc) = fc*twopi/ma.SR;
aapp(fc) = pow(ma.E, 0-omega(fc));
lp1p(g) = *(1-g):+~*(g);
integrator(s) = abs : lp1p(aapp(1/s));
process = _ : integrator(1);
```
### Spiegazione dell'Algoritmo

1. **Costante `twopi` e Funzione `omega(fc)`**:
   - La costante `twopi` viene definita come `2 * π`, dove `π` è una costante matematica rappresentante il rapporto tra la circonferenza di un cerchio e il suo diametro. 
   - La funzione `omega(fc)` calcola la frequenza angolare `omega` per una data frequenza di taglio `fc`, utilizzando la formula della trasformata bilineare.

2. **Funzione `aapp(fc)`**:
   - La funzione `aapp(fc)` applica la trasformata bilineare all'esponenziale negativo della frequenza angolare `omega(fc)`. Questa operazione è cruciale per la progettazione di filtri digitali, poiché stabilisce il comportamento del filtro nel dominio digitale rispetto alla sua controparte analogica.
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/camp0-svg/aapp-0x13e62b080.svg?ref_type=heads](docs/camp0-svg/aapp-0x13e62b080.svg)

3. **Filtro Passa Basso a un Polo (`lp1p(g)`)**:
   - Il filtro `lp1p(g)` è un filtro passa basso a un polo, dove `g` rappresenta il coefficiente di guadagno del filtro. Questo tipo di filtro è progettato per attenuare le alte frequenze mantenendo quelle basse.
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/camp0-svg/lp1p-0x13e6302e0.svg?ref_type=heads](docs/camp0-svg/lp1p-0x13e6302e0.svg)
4. **Integratore (`integrator(s)`)**
   - L'`integrator(s)` è un componente che calcola la media degli valori assoluti dei campioni di ingresso su finestre temporali specifiche.
   - Utilizza il filtro passa basso a un polo `lp1p` con il coefficiente `aapp(1/s)`, dove `s` rappresenta il periodo di integrazione espresso in secondi.
   ![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/camp0-svg/integrator-0x13e630450.svg?ref_type=heads](docs/camp0-svg/integrator-0x13e630450.svg)

## Algoritmo RMS

```faust
import("stdfaust.lib");
// Moving Average Envelope Follower
movingAverage(seconds, x) = x - x@(seconds * ma.SR) : 
    fi.pole(1) / (seconds * ma.SR);

// Moving Average RMS
movingAverageRMS(seconds, x) = sqrt(max(0, movingAverage(seconds, x ^ 2)));

process = _ : movingAverageRMS(0.01);
```

### Spiegazione dell'Algoritmo

**Moving Average Envelope Follower:** La funzione `movingAverage(seconds, x)` calcola una media mobile dell'ampiezza del segnale `x` nel tempo. Questo tipo di media mobile è utile per monitorare la variazione dell'ampiezza nel tempo, filtrando le variazioni a breve termine e evidenziando tendenze più lunghe nel segnale. In particolare:

- Utilizza un filtro pole-zero (`fi.pole(1)`) per applicare la media mobile.
- `seconds` rappresenta il periodo di tempo su cui viene calcolata la media mobile, espresso in secondi.
- `x - x@(seconds * ma.SR)` calcola la differenza tra il segnale originale e il segnale ritardato di `seconds` secondi.
- `fi.pole(1) / (seconds * ma.SR)` applica il filtro pole per ottenere la media mobile, considerando il tasso di campionamento del segnale (`ma.SR`).
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/algoritmoRMS-svg/movingAverage-0x141b24e30.svg?ref_type=heads](docs/algoritmoRMS-svg/movingAverage-0x141b24e30.svg)

**Moving Average RMS:** La funzione `movingAverageRMS(seconds, x)` calcola la radice quadrata della media mobile dell'energia del segnale `x`. Questo è un indicatore RMS (Root Mean Square) dell'ampiezza del segnale nel tempo:

- Utilizza la funzione `movingAverage` per calcolare la media mobile del segnale al quadrato (`x ^ 2`), assicurandosi che il risultato sia sempre non negativo con `max(0, ...)`.
- `sqrt()` calcola la radice quadrata della media mobile, fornendo una stima RMS dell'ampiezza del segnale.
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/algoritmoRMS-svg/movingAverageRMS-0x141b26270.svg?ref_type=heads](docs/algoritmoRMS-svg/movingAverageRMS-0x141b26270.svg)

### Applicazioni e Utilità

Questo tipo di approccio è particolarmente utile nelle applicazioni di elaborazione audio in tempo reale per diverse ragioni:

- **Monitoraggio della Dinamica**: La media mobile permette di monitorare la variazione dinamica del segnale audio nel tempo, facilitando il controllo del volume o l'implementazione di effetti basati sull'ampiezza.
  
- **Stabilizzazione dell'Analisi**: Calcolare l'ampiezza media su un periodo di tempo aiuta a stabilizzare l'analisi del segnale, riducendo il rumore e le fluttuazioni indesiderate.

- **Controllo Preciso**: La media mobile RMS fornisce una stima accurata dell'energia media del segnale, essenziale per applicazioni che richiedono una regolazione precisa della dinamica audio.

Questo approccio è cruciale per garantire una risposta controllata e precisa nell'elaborazione del segnale audio, adattabile a una vasta gamma di applicazioni audio e musicali.


## 4.4 Peak Envelope Follower adattivo

L'algoritmo presentato è composto da una serie di funzioni collegate in serie per manipolare e analizzare un segnale audio in tempo reale. Ecco una spiegazione dettagliata di come funziona questa composizione:
Utilizza un approccio basato su feedback per calcolare dinamicamente l'inviluppo del segnale, garantendo una risposta rapida ai picchi e una transizione graduale durante i periodi di basso livello del segnale.

```faust
import("stdfaust.lib");
peakenvelope(period, x) = loop ~ _
    with {
        loop(y) = max(abs(x), y * coeff);
        twoPIforT = (2.0 * ma.PI) * (1.0 / ma.SR);
        coeff = exp(-twoPIforT / max(ma.EPSILON, period));
    };
        
line(value, trigger) = (feedf ~ feedb)
 with 
    {   
        break = value;
        y = trigger > trigger';
        feedf(x) = (x > 0) + (x * (x > 0)) + y;
        feedb(x) = (x <= break) * x;
        offset(x) = ((x - 1) > 0) * (x - 1);
    };

diffTresh(x) = x > x';

time = 0.1;

process = _ <: (peakenvelope(time) > 0.10 : 
         (line(ma.SR*time) / (ma.SR*time)) > 0 : 
         diffTresh), _;
```

![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/contatorePicchi-svg/process.svg?ref_type=heads](docs/contatorePicchi-svg/process.svg)

### Composizione dell'Algoritmo

1. **Peakenvelope (`peakenvelope(period, x)`)**:
   - Questa funzione traccia il picco massimo dell'ampiezza del segnale `x` nel tempo utilizzando un *Peak Envelope Follower*. Calcola un inviluppo che segue i picchi del segnale in ingresso.
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/contatorePicchi-svg/peakenvelope-0x13d7ac4e0.svg?ref_type=heads](docs/contatorePicchi-svg/peakenvelope-0x13d7ac4e0.svg)
2. **Line (`line(value, trigger)`)**:
   - Questa funzione combina un processo di feedforward (`feedf`) e di feedback (`feedb`) con un parametro di interruzione (`break`). È utilizzato per generare un segnale basato su condizioni specifiche di valore e trigger.
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/contatorePicchi-svg/line-0x13d7b2fe0.svg?ref_type=heads](docs/contatorePicchi-svg/line-0x13d7b2fe0.svg)
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/contatorePicchi-svg/feedf-0x13d7b0c90.svg?ref_type=heads](docs/contatorePicchi-svg/feedf-0x13d7b0c90.svg)
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/contatorePicchi-svg/feedb-0x13d7b2df0.svg?ref_type=heads](docs/contatorePicchi-svg/feedb-0x13d7b2df0.svg)

3. **DiffTresh (`diffTresh(x)`)**:
   - Una funzione semplice che confronta ogni campione del segnale con il suo precedente per determinare se è superiore al suo valore precedente.
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/contatorePicchi-svg/diffTresh-0x13d7b42f0.svg?ref_type=heads](docs/contatorePicchi-svg/diffTresh-0x13d7b42f0.svg)
#### Codice Peakenvelope

```faust
peakenvelope(period, x) = loop ~ _
    with {
        loop(y) = max(abs(x), y * coeff);
        twoPIforT = (2.0 * ma.PI) * (1.0 / ma.SR);
        coeff = exp(-twoPIforT / max(ma.EPSILON, period));
    };
```
#### Dettagli dell'Implementazione

- **Loop di Feedback**:
  - La funzione `loop(y)` rappresenta il ciclo di feedback dell'algoritmo. In ogni iterazione, confronta il valore assoluto del segnale corrente `abs(x)` con il valore precedente attenuato `y * coeff`, dove `y` è l'inviluppo calcolato in precedenza.
  ![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/contatorePicchi-svg/loop-0x13d7ac2f0.svg?ref_type=heads](docs/contatorePicchi-svg/loop-0x13d7ac2f0.svg)

- **Coefficiente di Smorzamento (coeff)**:
  - `coeff` è un parametro che controlla la velocità di decadimento dell'inviluppo. Viene calcolato utilizzando il periodo di campionamento del segnale `period` e la costante `twoPIforT`, che rappresenta \( \frac{2 \pi}{T} \). Questo calcolo determina quanto rapidamente l'inviluppo si adatta ai cambiamenti nel segnale.
  ![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/contatorePicchi-svg/coeff-0x13d7ab000.svg?ref_type=heads](docs/contatorePicchi-svg/coeff-0x13d7ab000.svg)
#### Funzionamento Generale

- L'algoritmo inizia calcolando il valore assoluto del segnale di ingresso `x`.
- Utilizza un filtro o una funzione di smoothing per evitare oscillazioni indesiderate nell'inviluppo.
- Risponde velocemente ai picchi del segnale, mantenendo un'accurata rappresentazione dell'ampiezza massima nel tempo.


### Concatenazione delle Funzioni

Nell'algoritmo la concatenazione delle funzioni ha lo scopo di isolare il primo campione che supera una soglia specifica e di escludere i successivi campioni entro un intervallo di tempo di 0.1 secondi. Questo approccio è comune nelle applicazioni di elaborazione del segnale audio per vari motivi:

1. **Isolamento del Primo Campione Significativo**:
   - Utilizzando la funzione *Peakenvelope*, si traccia l'inviluppo dell'ampiezza del segnale. Quando l'ampiezza supera una determinata soglia (`0.10` nel nostro caso), il segnale passa attraverso la condizione `peakenvelope(time) > 0.10`.
   - Questo passaggio è cruciale per identificare il momento in cui il segnale supera il livello desiderato per la prima volta.

2. **Esclusione dei Campioni Successivi entro 0.1 Secondi**:
   - Dopo aver rilevato il superamento della soglia, il segnale viene processato attraverso la funzione `line(ma.SR*time) / (ma.SR*time) > 0 : diffTresh`.
   - La funzione `line(value, trigger)` combina un processo di feedforward e di feedback per generare un segnale che tiene conto di una specifica soglia di `value` e di un `trigger`.
   - Questo passaggio è progettato per escludere i successivi campioni che cadono al di sopra della soglia dopo il primo superamento entro l'intervallo di tempo specificato (`0.1` secondi).

3. **Applicazioni e Utilità**:
   - Spesso, l'attacco di un suono può avere più campioni sopra una certa soglia, e isolare solo il primo campione significativo aiuta a evitare artefatti indesiderati come *ripples* nell'inviluppo dei picchi (*peakenvelope*) quando la curva inizia a discendere.
   - La funzione `diffTresh` si attiva quindi una sola volta per ogni picco significativo, garantendo una risposta più stabile e precisa nell'analisi e nell'elaborazione del segnale audio.



## Envelope follower coniugato
Il codice FAUST qui presentato è progettato per rilevare il picco di un segnale audio e, utilizzando un ritardo con feedback, crea un effetto di smussamento che consente di avere una curva di rilascio di durata variabile. Ecco una spiegazione dettagliata di come funziona questo processo:
```faust
import("stdfaust.lib");

twopi = 2*ma.PI;
omega(fc) = fc*twopi/ma.SR;
aapp(fc) = pow(ma.E, 0-omega(fc));
lp1p(g) = *(1-g):+~*(g);
integrator(s) = abs : lp1p(aapp(1/s));

delayFbk(s,fbk) = (+ : de.delay(ma.SR, ba.sec2samp(s)-1))~*(fbk);

mapQ(x) = (1-x)^2;

camp0(at,ig) = _ <: de.delay(ma.SR,ba.sec2samp(at)) * (integrator(ig) : 
                     delayFbk(0.0001,0.94) : mapQ);

process = camp0(0.01,0.01);
```
![https://gitlab.com/consaq/sistemielinguaggi/-/blob/main/tesi/scrittura_libera/docs/camp0-svg/process.svg?ref_type=heads](docs/camp0-svg/process.svg)
Il modulo ```integratore``` è stato presentato recentemente.

Il modulo ```delayFbk``` introduce un ritardo con feedback nel segnale. Il ritardo è specificato in secondi \(s\), e ```fbk``` rappresenta il fattore di feedback. Il feedback riporta una parte del segnale ritardato all'ingresso, creando un effetto di smussamento.
![](docs/camp0-svg/delayFbk-0x13e63c520.svg)

# Conclusione

Il lavoro svolto ha permesso di esplorare e approfondire le competenze teoriche e pratiche nel campo del Signal Information Processing (SIP), con un focus particolare sull'utilizzo dei filtri per la manipolazione del segnale digitale e la costruzione di descrittori temporali come l'envelope following nelle sue varianti di *Peak Envelope* e *RMS*. L'utilizzo di FAUST come ambiente di sviluppo si è rivelato particolarmente efficace grazie alla sua natura open source, alla compatibilità multipiattaforma e alla possibilità di operare con precisione variabile. La transizione dalla teoria dei filtri analogici a quella dei filtri digitali è stata affrontata attraverso la trasformata bilineare, permettendo di comprendere e implementare filtri digitali con caratteristiche precise. Infine, l'analisi e la realizzazione di algoritmi per envelope followers ha mostrato la varietà e la complessità degli approcci possibili, evidenziando l'importanza di scegliere l'algoritmo più adatto in base alle caratteristiche del segnale e alle esigenze specifiche dell'applicazione. Questo percorso ha fornito una solida base per ulteriori sviluppi e applicazioni nel campo del live electronics e della manipolazione del segnale in tempo reale, promuovendo un atteggiamento ecosistemico e sostenibile nell'utilizzo delle tecnologie digitali.

# Bibliografia 
- Luca_Spanedda_Sistemi Complessi_Adattivi_per_la_performance_musicale_in_Live_Electronics
- Preserving the LTI system topology in s- to z-plane transforms
- The_Biquad_Part_I-Some_practical_design_considerations
- Towards_a_Typology_of_Feedback_Systems